#include <iostream>
#include <vector>
#include <stack>
using namespace::std;


//  Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    void reorderList(ListNode *head) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
		ListNode * newHead = NULL, *newTail = NULL, *tails = head;
		stack<ListNode *> t;
		if(!head || !head->next || !head->next->next)return;
		while(tails){
			t.push(tails);
			tails = tails->next;
		}
		vector<ListNode *> newLists;
        do{
			if(newHead == NULL){
				newLists = getHeadAndTail(head, t);
				newHead = newLists[0];
				newTail = newLists[1]->next;	
			}
			else{
				newLists = getHeadAndTail(newHead, t);
				if(!newLists[0])break;
				newHead = newLists[0];
				newTail->next = newLists[1];
				newTail = newLists[1]->next;
			}
		}while(newHead && newHead->next && newHead->next->next);
		newTail->next = newHead;
    }

	vector<ListNode*> getHeadAndTail(ListNode * n, stack<ListNode *> &s){
		ListNode * work = n, *last = NULL, *head = n;
		vector<ListNode *> res;
		last = s.top();
		s.pop();
		work = s.top();
		res.push_back(n->next);   // longer list
		res.push_back(n);         // shorter list
		n->next = last;
		work->next = NULL;
		return res;
	}
};

int main(){
	ListNode head(1);
	head.next = new ListNode(2);
	head.next->next = new ListNode(3);
	head.next->next->next = new ListNode(4);
	Solution s;
	s.reorderList(&head);
	return 0;
}