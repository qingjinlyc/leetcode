#include <iostream>
#include <unordered_map>
using namespace ::std;

//Definition for a point.
struct Point {
    int x;
    int y;
    Point() : x(0), y(0) {}
    Point(int a, int b) : x(a), y(b) {}
};
struct Line {
	int a;
	float b;
	float c;
};

class Solution {
public:
	int maxPoints(vector<Point> &points) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		unordered_map<float, float> lines_0, lines_1;
		float a = 0, b = 0, c = 0;
		int x1, y1, x2, y2;
		float x, y;
		int max = -1, count = 2;
		Line line;
		if(points.size() == 1)return 1;
		if(points.size() == 0)return 0;
		for(int i = 0; i < points.size() - 1; i++){
			for(int j = i + 1; j < points.size(); j++){
				count = 2;
				x1 = points[i].x;
				y1 = points[i].y;
				x2 = points[j].x;
				y2 = points[j].y;
				if(x1 == x2){
					line.a = 1;
					line.b = 0;
					line.c = (float)-x1;
					x = b + c;
					y = b - c;
				}
				else if(y1 == y2){
					line.a = 0;
					line.b = 1;
					line.c = (float)-y1;
					x = b + c;
					y = b - c;
				}
				else{
					b = ((float)x1 - (float)x2) / ((float)y2 - (float)y1);
					c = -((float)x1 + b * (float)y1);
					line.a = 1;
					line.b = b;
					line.c = c;
					x = b + c;
					y = b - c;
				}
				
// 				if(find(lines_0, lines_1, a, x, y))
// 					continue;
/*				else{*/
					for(int k = 0; k < points.size(); k++){
						if(k != i && k != j){
							if(points[k].x * line.a + (float)points[k].y * line.b + (float)line.c == 0){
								count++;
							}
						}
					}
					if(count > max)max = count;
// 					switch(a){
// 					case 0:lines_0.insert(pair<float, float>(x, y));
// 						break;
// 					case 1:lines_1.insert(pair<float, float>(x, y));
// 						break;
/*					}*/
/*				}*/
			}
		}
		return max;
	}
	bool find(unordered_map<float, float> line_0, unordered_map<float, float> line_1, int a, float x, float y){
		unordered_map<float, float> *work = NULL;
		float Y;
		switch(a){
		case 0:work = &line_0;
			break;
		case 1:work = &line_1;
			break;
		}
// 		try{
// 			Y = work->at(x);
// 			return (Y == y);
// 		}catch(out_of_range){
// 			return false;
// 		}
		unordered_map<float, float>::iterator it = work->find(x);
		if(it == work->end())return false;
		return it->second == y;
	}
};

int main(){
	vector<Point> p;
	p.push_back(*(new Point(-4, -4)));
	p.push_back(*(new Point(-8, -582)));
	p.push_back(*(new Point(-3, 3)));
	p.push_back(*(new Point(-9, -651)));
	p.push_back(*(new Point(9, 591)));
	Solution s;
	s.maxPoints(p);
	return 0;
}