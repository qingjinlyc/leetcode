#include <iostream>
#include <stack>
#include <vector>
using namespace::std;
 // Definition for binary tree
  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };
 
class Solution {
public:
    vector<int> preorderTraversal(TreeNode *root) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
        vector<int> res;
		stack<TreeNode *> s;
		TreeNode * work = NULL;
		if(root == NULL)return res;
		s.push(root);
		while(!s.empty()){
			work = s.top();
			s.pop();
			res.push_back(work->val);
			if(work->left == NULL && work->right){
				s.push(work->right);
				continue;
			}
			while(work->left){
				work = work->left;
				res.push_back(work->val);
				if(work->right)s.push(work->right);
			}
		}
		return res;
    }
};

int main(){
	TreeNode root(3);
	root.left = new TreeNode(1);
	root.left->right = new TreeNode(2);
	Solution s;
	s.preorderTraversal(&root);
	return 0;
}