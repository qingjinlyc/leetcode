#include <iostream>
#include <vector>
using namespace ::std;

class Solution {
public:
	int candy(vector<int> &ratings) {
		int res = 0;
		int last_num = 1, to_add = 1, to = 1, cur_state = 0, decline = 0;   // 0 for ascend, 1 for descend
		int i, j;
		if(ratings.size() == 1)return 1;
		else if(ratings.size() == 2){
			if(ratings[0] == ratings[1])return 2;
			else
				return 3;
		}
		res = 1;
		for(i = 1; i < ratings.size(); i++){
			if(ratings[i] > ratings[i - 1]){
				last_num++;
				res += last_num;
			}
			else if(ratings[i] == ratings[i - 1]){
				last_num = 1;
				res += last_num;
			}
			else{
				decline = 1;
				for(j = i + 1; j < ratings.size() && ratings[j] < ratings[j - 1]; j++)decline++;
				res += ((1 + decline) * decline / 2);
				if(decline >= last_num){
					res += (decline - last_num + 1);
				}
				last_num = 1;
				i = j - 1;
			}
		}
		return res;
	}
};
int main(){
	vector<int> rantings;
	rantings.push_back(1);
	rantings.push_back(2);
 	rantings.push_back(2);
// 	rantings.push_back(4);
// 	rantings.push_back(3);

	Solution s;
	int res = s.candy(rantings);
	return 0;
};