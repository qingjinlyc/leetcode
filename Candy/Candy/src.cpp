#include <iostream>
#include <vector>
using namespace ::std;

class Solution {
public:
	int candy(vector<int> &ratings) {
		int res = ratings.size();
		addcandy(ratings, 0, ratings.size() - 1, res);
		return res;
	}

	void addcandy(vector<int> &ratings, int start, int end, int & res){
		int min = 999999, min_pos = start;
		if(start >= end)return;
		for(int i = start; i <= end; i++){
			if(ratings[i] < min){
				min_pos = i;
				min = ratings[i];
			}
		}
		if(min_pos != start){
			if(min < ratings[min_pos - 1])res++;
		}
		if(min_pos != end){
			if(min < ratings[min_pos + 1])res++;
		}
		addcandy(ratings, start, min_pos - 1, res);
		addcandy(ratings, min_pos + 1, end, res);
	}
};

