#include <iostream>
#include <vector>
using namespace ::std;

class Solution {
public:
	int candy(vector<int> &ratings) {
		int res = ratings.size();
		vector<int> candies(res, 1);
		for(int i = 1; i < ratings.size(); i++){
			if(ratings[i] > ratings[i - 1]){
				int toadd = candies[i - 1] + 1 - candies[i];
				candies[i] = candies[i - 1] + 1;
				res += toadd;
			}
			else if(ratings[i] < ratings[i - 1]){
				if(candies[i] >= candies[i - 1]){
					int toadd = 0;
					candies[i - 1] += (toadd = (candies[i] - candies[i - 1] + 1));
					res += toadd;
					adjust(candies, i - 1, res, ratings);
				}
			}
		}
		return res;
	}

	void adjust(vector<int> &candies, int end, int &res, vector<int> &ratings) {
		for(int i = end; i > 0; i--){
			if(ratings[i] > ratings[i - 1])return;
			else if(ratings[i] < ratings[i - 1]){
				if(candies[i] >= candies[i - 1]){
					int toadd = 0;
					candies[i - 1] += (toadd = (candies[i] - candies[i - 1] + 1));
					res += toadd;
				}
				else
					return;
			}
		}
	}
};
