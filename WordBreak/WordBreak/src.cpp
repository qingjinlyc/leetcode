#include <iostream>
#include <unordered_set>
#include <string>

using namespace::std;

class Solution {
public:
	bool wordBreak(string s, unordered_set<string> &dict) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		return cansplit(s, dict);
	}

	bool cansplit(string s, unordered_set<string> &dict){
		bool res = false;
		int work = s.length() - 1;
		if(dict.find(s) != dict.end())return true;
		if(s.length() <= 1)return dict.find(s) != dict.end();
		for(; work > 0; work--){
			if(dict.find(s.substr(work)) != dict.end())
				res = res || cansplit(s.substr(0, work), dict);
		}
		return res;
	}
};

int main(){
	unordered_set<string> dict;
	dict.insert("aaaa");
	dict.insert("aaa");
	Solution s;
	s.wordBreak("aaaaaaa", dict);
	return 0;
}