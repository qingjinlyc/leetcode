#include <iostream>
#include <vector>
#include <string>
#include <stack>
#include <stdlib.h>
using namespace ::std;

class Solution {
public:
    int evalRPN(vector<string> &tokens) {
        int res = 0, left = 0, right = 0;
        vector<string>:: iterator it = tokens.begin();
		stack<int> operand;
		for(; it != tokens.end(); it++){
			if(isNumber(*it))operand.push(atoi((*it).c_str()));
			else{
				right = operand.top();
				operand.pop();
				left = operand.top();
				operand.pop();
				switch((*it).c_str()[0]){
				case '+':
					operand.push(left + right);
					break;
				case '-':
					operand.push(left - right);
					break;
				case '*':
					operand.push(left * right);
					break;
				case '/':
					operand.push(left / right);
					break;
				}
			}
		}
		return operand.top();
    }
	bool isNumber(string str){
		return !(str == "+" || str == "-" ||
			str == "*" || str == "/");
	}
};



int main(){
	vector<string> str;
	str.push_back("-1");
	str.push_back("1");
	str.push_back("*");
	str.push_back("-1");
	str.push_back("+");
	Solution s;
	int res = s.evalRPN(str);
	return 0;
}