class Solution {
public:
    int maxProfit(vector<int> &prices) {
        if(prices.size() == 0)return 0;
        vector<int> maxes;
        int flag = prices[0];
        int max = 0;
        for(int i = 1; i < prices.size(); i++) {
            if(prices[i] >= flag) {
                if(prices[i] - flag > max)max = prices[i] - flag;
            }
            else {
                flag = prices[i];
            }
        }
        return max;
    }
};