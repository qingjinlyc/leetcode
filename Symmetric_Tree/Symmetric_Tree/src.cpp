#include <iostream>
#include <stack>
#include <deque>

using namespace :: std;



struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    bool isSymmetric(TreeNode *root) {
        int last, cur, count;
        deque<TreeNode *> q;
        stack<TreeNode *> s;
        if(!root)return true;
        q.push_back(root);
        cur = 0;
        last = 1;
        while(!q.empty()){
            TreeNode * n = q.front();
            q.pop_front();
            last--;
            if(n){
                q.push_back(n->left);
                cur++;
                q.push_back(n->right);
                cur++;
            }
            if(last == 0){
                count = cur / 2;
                int i = 0;
                while(count){
                    s.push(q[i++]);
                }
                while(!s.empty()){
                    TreeNode * n2 = s.top(), * n3 = q[i++];
                    s.pop();
                    if((!n2 && n3) || (n2 && !n3))return false;
                    if(n2){
                        if(n2->val != n3->val)return false;
                    }
                }
                last = cur;
                cur = 0;
            }
        }
        return true;
    }
};

int main(){
	TreeNode n(1);
	Solution s;
	bool res = s.isSymmetric(&n);
	return 0;
}