#include <iostream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <queue>
using namespace ::std;


struct UndirectedGraphNode {
	   int label;
	   vector<UndirectedGraphNode *> neighbors;
	   UndirectedGraphNode(int x) : label(x) {};
};
	
class Solution {
public:
	UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
		if(!node)return NULL;
		vector<UndirectedGraphNode *> allnodes;
		UndirectedGraphNode *newgraph = NULL;
		unordered_set<UndirectedGraphNode *> cloned;
		queue<UndirectedGraphNode *> toclone;
		unordered_map<UndirectedGraphNode *, UndirectedGraphNode *> nodemap;
		toclone.push(node);
		while(!toclone.empty()){
			UndirectedGraphNode * n = toclone.front();
			toclone.pop();
			if(cloned.find(n) == cloned.end()){
				nodemap.insert(pair<UndirectedGraphNode *, UndirectedGraphNode *>(n, new UndirectedGraphNode(n->label)));
				allnodes.push_back(n);
				for(vector<UndirectedGraphNode *> :: iterator it = n->neighbors.begin(); it != n->neighbors.end(); it++){
					toclone.push(*it);
				}
				cloned.insert(n);
			}
		}
		for(vector<UndirectedGraphNode *> :: iterator it = allnodes.begin(); it != allnodes.end(); it++){
			if(it == allnodes.begin()){
				newgraph = nodemap.find(*it)->second;
				for(vector<UndirectedGraphNode *> ::iterator it_neighbor = (*it)->neighbors.begin(); it_neighbor != (*it)->neighbors.end(); it_neighbor++){
					newgraph->neighbors.push_back(nodemap.find(*it_neighbor)->second);
				}
			}
			else{
				UndirectedGraphNode * node = nodemap.find(*it)->second;
				for(vector<UndirectedGraphNode *> ::iterator it_neighbor = (*it)->neighbors.begin(); it_neighbor != (*it)->neighbors.end(); it_neighbor++){
					node->neighbors.push_back(nodemap.find(*it_neighbor)->second);
				}
			}
		}
		return newgraph;
	}
};

int main(){
	UndirectedGraphNode g(0);
	g.neighbors.push_back(new UndirectedGraphNode(1));
	g.neighbors[0]->neighbors.push_back(new UndirectedGraphNode(2));
	g.neighbors[0]->neighbors.push_back(g.neighbors[0]->neighbors[0]);
// 	g.neighbors[0]->neighbors.push_back(new UndirectedGraphNode(23));
// 	g.neighbors[0]->neighbors.push_back(new UndirectedGraphNode(11));
// 	g.neighbors[0]->neighbors.push_back(new UndirectedGraphNode(12));
// 
// 	g.neighbors[1]->neighbors.push_back(new UndirectedGraphNode(1));
// 	g.neighbors[1]->neighbors.push_back(new UndirectedGraphNode(12));
// 	g.neighbors[1]->neighbors.push_back(new UndirectedGraphNode(46));
	Solution s;
	UndirectedGraphNode * res = s.cloneGraph(&g);
	return 0;
}