#include <iostream>
#include <vector>
#include <string>
using namespace ::std;

class Solution {
public:
	vector<vector<string> > solveNQueens(int n) {
		vector<int> canplace(n, 1);
		vector<vector<string>> res;
		vector<string> cur_line;
		place(res, 0, canplace, cur_line, n, -1, -1);
		return res;
	}
	void place(vector<vector<string>> & lines, int line, vector<int> & canplace, vector<string> & cur_line, int n, int left, int right) {
		string pos(n, '.');
		int new_left = -1, new_right = -1;
		if(line == n)return;
		for(int i = 0; i < n; i++) {
			if(canplace[i] && !onslash(cur_line, i, line, n)) {
				pos[i] = 'Q';
				canplace[i] = 0;
				if(i > 0) {
					new_left = i - 1;
				}
				if(i < n - 1) {
					new_right = i + 1;
				}

				cur_line.push_back(pos);
				place(lines, line + 1, canplace, cur_line, n, new_left, new_right);
				if(line == n - 1)
					lines.push_back(cur_line);
				canplace[i] = 1;
				pos[i] = '.';
				cur_line.erase(cur_line.begin() + line);
			}
		}
	}

	bool onslash(vector<string> cur_line, int col, int line, int n) {
		for(int k = 0; k < cur_line.size(); k++)
			for(int j = 0; j < cur_line[k].size(); j++)
				if(cur_line[k][j] == 'Q' && (col - j == line - k || j - col == line - k))return true;
		return false;
	}
};

int main() {
	Solution s;
	vector<vector<string>> res = s.solveNQueens(8);
	return 0;
}