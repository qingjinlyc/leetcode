#include <stack>
using namespace ::std;

/**
* Definition for binary tree
*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
	int sumNumbers(TreeNode *root) {
		stack<TreeNode *> s;
		stack<int> cur_sum;
		if (!root)return 0;
		s.push(root);
		int sum = 0;
		cur_sum.push(0);
		while (!s.empty()) {
			TreeNode * n = s.top();
			int cur_rt = cur_sum.top();
			s.pop();
			cur_sum.pop();
			if (!(n->val == 0 && !cur_rt)) {
				cur_rt += (cur_rt * 10 + n->val);
			}
			if (!n->left && !n->right) {
				sum += (cur_rt);
			}
			if (n->right) {
				s.push(n->right);
				cur_sum.push(cur_rt);
			}
			if (n->left) {
				s.push(n->left);
				cur_sum.push(cur_rt);
			}
		}
		return sum;
	}
};

int main(){
	Solution s;
	TreeNode * n = new TreeNode(1);
	n->left = new TreeNode(0);
	int res = s.sumNumbers(n);
	return 0;
}