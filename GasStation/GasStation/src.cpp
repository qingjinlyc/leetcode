#include <iostream>
#include <vector>

using namespace:: std;

class Solution {
public:
	int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {
		// IMPORTANT: Please reset any member data you declared, as
		// the same Solution instance will be reused for each test case.
		vector<int> weights;
		vector<int>::iterator it_gas = gas.begin(), it_cost = cost.begin();
		for(; it_gas != gas.end() && it_cost != cost.end(); it_gas++, it_cost++)
			weights.push_back(*it_gas - *it_cost);
		int start = 0, start_work = 0, sum = 0, len = cost.size(), i = 0;
		if(len == 1)
			return cost[0] > gas[0] ? -1: 0;
		while(i < len){
			sum += weights[start_work++];
			i++;
			start_work %= len;
			if(start_work == 0){
				if(sum < 0)return -1;
				else if(weights[start] == 0)
					return -1;
				else
					return start;
			}
			if(start_work == start)
				if(sum >= 0)return weights[start] == 0? -1: start;;
			if(sum < 0){
				sum = 0;
				start = start_work;
			}
		}
	}
};

int main(){
	vector<int> gas, cost;
	gas.push_back(2);
	gas.push_back(4);


	cost.push_back(3);
	cost.push_back(4);

	Solution s;
	int res = s.canCompleteCircuit(gas, cost);
	return 0;
}