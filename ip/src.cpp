#include <vector>
#include <string>

using namespace :: std;

class Solution {
public:
    vector<string> restoreIpAddresses(string s) {
        vector<string> res;
        string ip; 
        int f, se, t, len = s.length(), sps = 0, spt = 0;
        int up[3] = {3, 3, 3};
        if(s[0] == '0')up[0] = 1;
        for(f = 1; f <= up[0] && f - 1 < len - 1; f++) {
            up[1] = 3;
            up[2] = 3;
            ip = "";
            string fs = s.substr(0, f);
            if(f == 3 && fs.compare("255") > 0)continue;
			string temp_ss = ip;
            ip += (fs + ".");
			sps = f;
			if(s[sps] == '0')up[1] = 1;
			else
				up[1] = 3;
            for(se = 1; se <= up[1] && sps + se - 1 < len - 2; se++) {
                string ss = s.substr(sps, se);
                if(se == 3 && ss.compare("255") > 0)continue;
				string temp_s = ip;
                ip += (ss + ".");
				spt = sps + se;
				if(s[spt] == '0')up[2] = 1;
				else
					up[2] = 3;
                for(t = 1; t <= up[2] && spt + t - 1 < len - 1; t++) {
                    string ts = s.substr(spt, t);
                    if(t == 3 && ts.compare("255") > 0)continue;
                    string ls = s.substr(spt + t);
					if(ls.length() > 1 && ls[0] == '0')continue;
                    if(ls.length() > 3)continue;
                    if(ls.length() == 3 && ls.compare("255") > 0)continue;
					string temp_t = ip;
                    ip += (ts + ".");
                    ip += ls;
                    res.push_back(ip);
					ip = temp_t;
                }
				ip = temp_s;
            }
			ip = temp_ss;
        }
        return res;
    }
};

int main() {
	string ip = "10101010";
	Solution s;
	vector<string> res = s.restoreIpAddresses(ip);
	return 0;
}