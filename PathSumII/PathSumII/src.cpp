#include <vector>
#include <stack>


using namespace ::std;

/**
* Definition for binary tree
*/
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
	vector<vector<int> > pathSum(TreeNode *root, int sum) {
		int rlsum = 0;
		vector<vector<int> > res;
		vector<int> path;
		stack<pair<TreeNode *, pair<vector<int>, int>>> nodes;
		if (!root)
			return res;
		nodes.push(pair<TreeNode *, pair<vector<int>, int>>(root, pair<vector<int>, int>(path, 0)));
		while (!nodes.empty()) {
			TreeNode * n = nodes.top().first;
			int val = n->val;
			vector<int> cur_path = nodes.top().second.first;
			int cur_sum = nodes.top().second.second;
			nodes.pop();
			if (cur_sum + val != sum && !n->left && !n->right)continue;
			cur_path.push_back(n->val);
			if (cur_sum + val == sum)
				if (!n->left && !n->right) {
					res.push_back(cur_path);
				}
			if (n->right)nodes.push(pair<TreeNode *, pair<vector<int>, int>>(n->right, pair<vector<int>, int>(cur_path, cur_sum + val)));
			if (n->left)nodes.push(pair<TreeNode *, pair<vector<int>, int>>(n->left, pair<vector<int>, int>(cur_path, cur_sum + val)));
		}
		return res;
	}
};

int main(){
	TreeNode root(-2);
	root.right = new TreeNode(-3);
	root.right->left = new TreeNode(3);
	Solution s;
	vector<vector<int> > res = s.pathSum(&root, -2);
	return 0;
}