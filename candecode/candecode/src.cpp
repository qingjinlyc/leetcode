#include <iostream>
#include <unordered_map>
#include <string>

using namespace ::std;

class Solution {
public:
	unordered_map<int, int> hascount;
	Solution() {
		hascount.clear();
	}
	int numDecodings(string s) {
		if(s == "")return 0;
		return candecode(s, 0);
	}

	int candecode(string s, int index) {
		unordered_map<int, int>:: iterator it;
		int can = 0;
		if((it = hascount.find(index)) != hascount.end())
			return it->second;
		if(index == s.length()) {
			return 1;
		}
		string s1 = s.substr(index, 1);
		if(s1 != "0") {
			can = candecode(s, index + 1);
		}
		string s2 = "";
		if(index < s.length() - 1)
			s2 = s.substr(index, 2);
		if(s2 != "" && s2.compare("26") <= 0 && s2.compare("10") >= 0) {
			can += candecode(s, index + 2);
		}
		hascount.insert(pair<int, int>(index, can));
		return can;
	} 
};

int main(){
	Solution s;
	int ret = s.numDecodings("10");
	return 0;
}