#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <string>
using namespace::std;

struct node{
	bool dead;
	string str;
	vector<node> children;
	node():dead(true), str(""){};
};
class Solution {
public:
	vector<string> res;
	Solution(){
		res.clear();
	}
	vector<string> wordBreak(string s, unordered_set<string> &dict) {
		if(s.length() == 1){
			if(dict.find(s) != dict.end()){
				res.push_back(s);
				return res;
			}
			else
				return res;
		}
		node N;
		N.dead = false;
		N.str = s;
		unordered_map<string, bool> hasdealed;
		canSplit(N, s, dict, hasdealed);
		string temp_str = "";
		for(int i = 0; i < N.children.size(); i++){
			addRes(res, N.children[i], temp_str);
			temp_str = "";
		}
		return res;
	}

	bool canSplit(node & n, string s, unordered_set<string> &dict, unordered_map<string, bool> & hasdealed){
		if(s == "")return true;
		unordered_map<string, bool>::iterator it;
		if((it = hasdealed.find(s)) != hasdealed.end()){
			if(!it->second){
				return it->second;
			}	
		}
		bool cansplit = false;
		if(s.length() == 1){
			if(dict.find(s) != dict.end()){
				node N;
				N.str = s;
				N.dead = false;
				n.children.push_back(N);
				hasdealed.insert(pair<string, bool>(s, true));
				return true;
			}
			else{
				hasdealed.insert(pair<string, bool>(s, false));
				return false;
			}
		}
		for(int i = 1; i <= s.length(); i++){
			if(dict.find(s.substr(0, i)) != dict.end()){
				node N;
				N.str = s.substr(0, i);
				if(canSplit(N, s.substr(i), dict, hasdealed)){
					cansplit = true;
					N.dead = false;
					n.children.push_back(N);
				}
			}
		}
		if(cansplit){
			n.dead = false;
			hasdealed.insert(pair<string, bool>(s, cansplit));
			return cansplit;
		}
		hasdealed.insert(pair<string, bool>(s, false));
		return false;
	}

	void addRes(vector<string> & res, node N, string & s){
		if(N.children.size() != 0){
			s += (N.str + " ");
			for(int i = 0; i < N.children.size(); i++){
				string new_s = s;
				addRes(res, N.children[i], new_s);
			}
		}
		else
			res.push_back(s + N.str);
	}

};

int main(){
	string s("aaaaaaa");
	unordered_set<string> dict;
	dict.insert("aaaa");
	dict.insert("aa");
	dict.insert("a");
// 	dict.insert("sand");
// 	dict.insert("dog");
	Solution S;
	vector<string> res = S.wordBreak(s, dict);
	return 0;
}