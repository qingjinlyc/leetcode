#include <iostream>
#include <unordered_map>
using namespace::std;

struct RandomListNode {
     int label;
     RandomListNode *next, *random;
     RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
};

class Solution {
public:
	RandomListNode *copyRandomList(RandomListNode *head) {
		if(!head) return NULL;
		unordered_map<RandomListNode *, RandomListNode *> maps;
		RandomListNode * newHead = new RandomListNode(head->label), *work = NULL, *work_new = NULL;
		newHead->next = head->next;
		newHead->random = head->random;
		maps.insert(pair<RandomListNode*, RandomListNode*>(head, newHead));
		work = head->next;
		work_new = newHead;
		while(work){
			work_new->next = new RandomListNode(work->label);
			maps.insert(pair<RandomListNode*, RandomListNode*>(work, work_new->next));
			work = work->next;
			work_new = work_new->next;
		}
		work_new = newHead;
		work = head;
		while(work){
			RandomListNode * random = NULL;
			unordered_map<RandomListNode*, RandomListNode*>::iterator it = maps.find(work->random);
			if(it == maps.end()){
				work_new->random = NULL;
				work = work->next;
				work_new = work_new->next;
				continue;
			}
			work_new->random = it->second;
			work = work->next;
			work_new = work_new->next;
		}
		return newHead;
	}
};

int main(){
	RandomListNode head(-1);
	head.next = new RandomListNode(-1);
	head.random = head.next;
	Solution s;
	RandomListNode * res = s.copyRandomList(&head);
	return 0;
}