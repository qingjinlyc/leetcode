#include <iostream>
#include <string>
#include <unordered_map>
using namespace ::std;

class Solution {
public:
	string minWindow(string S, string T) {
		int min = 9999999;
		int min_end = 0, min_begin = 0;
		int start = 0, end = 0;
		int count = 0, len = T.length();
		unordered_map<char, int> hasFind, needFind;
		for(int i = 0; i < T.length(); i++) {
			unordered_map<char, int>:: iterator it = needFind.find(T[i]);
			if(it == needFind.end()) { 
				needFind.insert(pair<char, int>(T[i], 1));
				hasFind.insert(pair<char, int>(T[i], 0));
			}
			else
				it->second++;
		}
		unordered_map<char, int>::iterator pos;
		int hf = 0, sf = 0;
		for(; end < S.length(); end++) {
			if((pos = needFind.find(S[end])) != needFind.end()) {
				hf = ++(hasFind.find(pos->first)->second);
				if(hf <= pos->second) {
					count++;
				}
				if(count == len) {
					while(hasFind.find(S[start]) == hasFind.end() || 
						hasFind.find(S[start])->second > needFind.find(S[start])->second) {
							if(hasFind.find(S[start]) != hasFind.end())
								hasFind.find(S[start])->second--;
							start++;
					}
					if(min > end - start + 1) {
						min = end - start + 1;
						min_end = end;
						min_begin = start;
					}
				}
			}
		}
		if(count != len)return "";
		return S.substr(min_begin, min_end - min_begin + 1);
	}
};

int main(){
	Solution s;
	string res = s.minWindow("ab", "b");
	return 0;
}